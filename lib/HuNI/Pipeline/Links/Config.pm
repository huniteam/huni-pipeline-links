package HuNI::Pipeline::Links::Config;

use 5.014_000; # This needs to run on agg01 right now, which has perl 5.14.2
use warnings;

use Carp                    qw( croak );
use Function::Parameters    qw( :strict );
use Log::Any                qw( $log );
use Text::CSV::Slurp        qw( );

use Moo;

has filename => (
    is => 'ro',
    predicate => 1,
);

has config => (
    is => 'lazy',
);

method find_linktype($link) {
    for my $row (@{ $self->config }) {
        if ($self->_match($link, $row)) {
            return if $row->{ignored};

            $link->{link_type} = $row->{link_type};
            $link->{pair_type} = $row->{pair_type};
            return $link;
        }
    }
    return;
}

method _match($link, $row) {

    my @match_fields = qw( from_feed from_type to_feed to_type );
    for my $field (@match_fields) {
        return unless $link->{$field} eq $row->{$field};
    }
    return ($row->{relationship} eq '')
        || ($row->{relationship} eq $link->{type});
}

method _build_config {
    croak 'Missing parameter filename' unless $self->has_filename;
    my $config = Text::CSV::Slurp->load(file => $self->filename);
    $self->_validate_config($config);
    return $config;
}

method _validate_config($config) {
    my $line = 2;
    my $errors = 0;
    for my $row (@$config) {
        $errors += $self->_validate_row($line++, $row);
    }
    die "Bailing out.\n" if $errors;
}

method _validate_row($line, $row) {
    my $errors = 0;

    $errors += $self->_validate_field($line, $row, $_)
        for qw( from_feed from_type to_feed to_type );

    if (!$row->{ignored}) {
        $errors += $self->_validate_field($line, $row, $_)
            for qw( link_type pair_type );
    }

    return $errors;
}

method _validate_field($line, $row, $field) {
    if (!$row->{$field}) {
        $log->error(sprintf('%s(%d): missing field "%s"',
            $self->filename, $line, $field));
        return 1;
    }
    return 0;
}

1;
