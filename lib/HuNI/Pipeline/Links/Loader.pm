package HuNI::Pipeline::Links::Loader;

use 5.014_000; # This needs to run on agg01 right now, which has perl 5.14.2
use warnings;

use Function::Parameters        qw( :strict );
use Log::Any                    qw( $log );
use Path::Tiny                  qw( path );
use XML::Simple                 qw( :strict );

use Moo;

has paths => (
    is => 'ro',
    default => sub { [ ] },
);

has links => (
    is => 'ro',
    default => sub { [ ] },
);

around BUILDARGS => fun($orig, $class, @args) {
    return $class->$orig(paths => [ map { path($_) } @args ]);
};

method next {
    return unless $self->load_more_links;
    return shift @{ $self->links };
}

method load_more_links {
    return 1 if @{ $self->links };

    return unless $self->load_more_files;

    my $path = shift @{ $self->paths };
    my $data = XMLin($path->slurp_utf8,
        ForceArray  => [ 'link', 'item' ],
        KeyAttr     => { item => 'name' },
    );

    my @links = @{ $data->{link} };
    for my $link (@links) {
        $link->{from_docid} = $data->{from_docid};

        # Anything but a string gtfo
        delete $link->{synopsis} if ref $link->{synopsis};
        $link->{synopsis} //= '';
        $link->{type} //= '';
    }

    push(@{ $self->links }, @links);

    return 1;
}

method load_more_files {
    while (1) {
        return unless @{ $self->paths };
        return 1 if $self->paths->[0]->is_file;

        my $dir = shift @{ $self->paths };
        push(@{ $self->paths }, $dir->children);
    }
}

1;
