#requires 'perl', '5.008001';

on 'test' => sub {
    requires 'Test::More', '0.98';
};

requires $_ for qw(
    Cpanel::JSON::XS
    Data::Dumper::Concise
    Function::Parameters
    Getopt::Args
    HTTP::Tiny
    Log::Any
    Moo
    Path::Tiny
    Text::CSV::Slurp
    Time::Elapsed
    XML::Simple
);

requires 'HTTP::Tiny',  '0.070';
requires 'Net::SSLeay', '1.80';
